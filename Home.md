# Руководство пользователя

Данное руководство позволяет начать работу с системой контроля версий Git

## Git

Для начала необходимо скачать и установить [Git](http://git-scm.com/) с официального сайта.

Там же можно ознакомиться с подробной [документацией](http://git-scm.com/book/ru/)

Устанавливать Git рекомендуется с параметрами по умолчанию.

## Bitbucket

Bitbucket позволяет работать с репозитариями по таким протоколам как HTTPS и SSH. Поскольку HTTPS требует аутентификации по паролю, а SSH по ключу, выбор в пользу SSH очевиден.

## Генерация ключей SSH

Для того чтобы работать с ssh из консоли windows используя аутентификацию по ключу, необходимо создать в папке профиля директорию **.ssh**

```
> mkdir %USERPROFILE%\.ssh
```

Для генерации ключей под windows будем  использовать программу puttygen из программного пакета [putty](http://www.putty.org/). Для генерации ключей под Linux, стыдно не знать как это [делается](https://wiki.archlinux.org/index.php/%D0%9A%D0%BB%D1%8E%D1%87%D0%B8_SSH).

![1.png](https://bitbucket.org/repo/Gpnne7/images/3517753605-1.png)

Длины ключа размерностью 2048 в большинстве случаев будет достаточно. Но для истинных параноиков можно выставить 4096 и даже 8192 что скажется не только на времени генерации, но и шифровании трафика в целом.

![2.png](https://bitbucket.org/repo/Gpnne7/images/3183755264-2.png)

**Key comment** необязательное поле и его можно не указывать.

**Key passphrase** и **Confirm passphrase** рекомендую не указывать, т.к. при каждом обращении к ключу будет запрашиваться пароль.

Сохраните в созданную директорию **.ssh** public и private ключи. Затем экспортируйте private openssh key (Conversions - Export OpnSSH key). Рекомендуется давать имена следующим образом:

* **public** - keyname.pub

* **private** - keyname.ppk

* **private openssh** - id_rsa 

id_rsa - это имя приватного ключа по умолчанию, именно по этому имени ssh-клиент будет обращаться к ключу.


Wiki pages are normal files, with the .md extension. You can edit them locally, as well as creating new ones.

## Syntax highlighting


You can also highlight snippets of text (we use the excellent [Pygments][] library).

[Pygments]: http://pygments.org/


Here's an example of some Python code:

```
#!python

def wiki_rocks(text):
    formatter = lambda t: "funky"+t
    return formatter(text)
```


You can check out the source of this page to see how that's done, and make sure to bookmark [the vast library of Pygment lexers][lexers], we accept the 'short name' or the 'mimetype' of anything in there.
[lexers]: http://pygments.org/docs/lexers/


Have fun!